from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf.urls import url

app_name = 'messagess'
app_name = 'api'


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
]


schema_view = get_schema_view(
   openapi.Info(
      title="Message API",
      default_version='v1',
      description="Документация для сервиса уведомлений",
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns += [
   url(r'^docs(?P<format>\.json|\.yaml)$', 
       schema_view.without_ui(cache_timeout=0), name='schema-json'),
   url(r'^docs/$', schema_view.with_ui('swagger', cache_timeout=0), 
       name='schema-swagger-ui'),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
