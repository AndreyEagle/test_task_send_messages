
import datetime as dt

from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone



class Tag(models.Model):
    tag = models.CharField(
        max_length=10
    )

    def __str__(self):
        return self.tag

class Code(models.Model):
    operator_code = models.CharField(
        verbose_name='Код мобильного оператора',
        max_length=3
    )

    def __str__(self):
        return self.operator_code

class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r'^(?:7)?9(?:\d{9})$', 
        message="Телефон должен иметь формат: '7XXXXXXXX'"
        )
    number_phone = models.CharField(
        validators=[phone_regex],
        max_length=11,
        unique=True
    )
    operator_code = models.ForeignKey(
        Code,
        on_delete=models.CASCADE,
        related_name='client_code'
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
        related_name='client_tag'
    )
    #operator_code = models.CharField(
    #    verbose_name='Код мобильного оператора',
    #    max_length=3
    #)
    #tag = models.CharField(max_length=15)
    time_zone = models.DateTimeField(default=dt.datetime.now())


class Message(models.Model):
    #text = models.TextField(max_length=256)
    created = models.DateTimeField(
        'Дата и время отправки', 
        auto_now_add=True
    )
    status = models.CharField(max_length=30)
    #distrib = models.ForeignKey(
    #    Distribution,
    #    on_delete=models.CASCADE
    #)
    distrib = models.CharField(
        max_length=30
    )
    client = models.CharField(max_length=30)

    def __str__(self):
        return self.text

class Distribution(models.Model):
    mailing_time = models.DateTimeField(
        'Дата рассылки',
        default=timezone.now
    )
    msg = models.TextField(
        max_length=256
    )
    mailing_end = models.DateTimeField(
        'Время окончания рассылки',
        default=timezone.now
    )
    client_tag = models.ManyToManyField(
        Tag,
        related_name='clients_tag'
    )
    client_code = models.ManyToManyField(
        Code,
        related_name='clients_code'
    )
