from rest_framework import serializers
from django.shortcuts import get_object_or_404, get_list_or_404
from messagess.models import Distribution, Client, Message, Tag, Code
#from send_message_api.messagess.models import DistribMessage
#import datetime as dt

class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = (
            'id',
            'text',
            'client',
            'disrib'
        )
    
    #def create(self, validated_data):
    #    mesage = Message.objects.create()
    #    return mesage

class TagSerializer(serializers.ModelSerializer):
   # tag = serializers.SlugRelatedField(
        #queryset = Tag.objects.all(),
      #  slug_field='tag',
       # read_only=True,
       # many=True
    #)
    class Meta:
        model = Tag
        fields = (
            'tag',
        )
    


class CodeSerializer(serializers.ModelSerializer):
    #operator_code = serializers.SlugRelatedField(
        #queryset = Code.objects.all(),
        #slug_field='operator_code',
        #read_only=True,
        #many=True
    #)
    class Meta:
        model = Code
        fields = (
            'operator_code',
        )

class DistributionSerializer(serializers.ModelSerializer):
    mailing_time = serializers.DateTimeField()
    mailing_end = serializers.DateTimeField()
    #client_tag = TagSerializer(many=True)
    #client_code = CodeSerializer(many=True)
    client_tag = serializers.SlugRelatedField(
        #queryset = Tag.objects.all(),
        slug_field='tag',
        read_only=True,
        many=True
    )
    client_code = serializers.SlugRelatedField(
        #queryset = Code.objects.all(),
        slug_field='operator_code',
        read_only=True,
        many=True
    )
    class Meta:
        fields = (
            'id',
            'mailing_time',
            'msg',
            'mailing_end',
            'client_tag',
            'client_code'
        )
        model = Distribution

    #def create(self, validated_data):
       # filtertag = validated_data.pop('client_tag')
        #filtercode = validated_data.pop('client_code')
        #distrib = Distribution.objects.create(**validated_data)
        #for tag in filtertag:
            #current_tag = get_object_or_404(Tag, **tag)
         #   distrib.client_tag.create(**tag)
        #for code in filtercode:
            #current_code = get_object_or_404(Code, **code)
         #   distrib.client_code.create(**code)
        #    filter_tag=client,
        #    filter_code=code,
         #   msg=msg,
        #)

        #return distrib

    def validate(self, data):
        if data['mailing_time'] > data['mailing_end']:
            raise serializers.ValidationError(
                'Время окончания рассылки должно быть больше времени начала'
            )
        return data


class ClientSerializer(serializers.ModelSerializer):
    operator_code = serializers.StringRelatedField()
    tag = serializers.StringRelatedField()

    class Meta:
        fields = (
            'id',
            'number_phone',
            'operator_code',
            'tag', 
            'time_zone'
        )
        model = Client

    def create(self, validated_data):
        tag = Tag.objects.create(tag=self.initial_data['tag'])
        code = Code.objects.create(operator_code=validated_data['number_phone'][1:4])
        client = Client.objects.create(
            number_phone=validated_data['number_phone'],
            tag=tag,
            operator_code=code
        )
        return client
