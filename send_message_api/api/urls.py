from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import DistributionViewSet, ClientViewSet


app_name = 'messagess'
app_name = 'api'

router = DefaultRouter()
router.register(r'v1/distribution', DistributionViewSet)
router.register(r'v1/client', ClientViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
