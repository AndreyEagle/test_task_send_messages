#from django.shortcuts import get_object_or_404
from ast import operator

from django.shortcuts import get_object_or_404, get_list_or_404
from messagess.models import Distribution, Client, Tag, Code
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

#from .permissions import CheckingUserIsAuthor

from .serializers import DistributionSerializer, ClientSerializer
#from messagess.tasks import send_msg

class DistributionViewSet(viewsets.ModelViewSet):

    #permission_classes = (IsAuthenticated,)

    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer

    def perform_create(self, serializer):
        tag = get_list_or_404(Tag, tag=self.request.data['client_tag'])
        code = get_list_or_404(Code, operator_code=self.request.data['client_code'])
        serializer.save(
            client_tag=tag,
            client_code=code
        )

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

